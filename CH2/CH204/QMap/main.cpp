﻿#include <QCoreApplication>
#include<QDebug>

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	QMap<QString, QString> map;
	map.insert("beijing", "111");
	map.insert("shanghai", "021");
	map.insert("nanjing", "025");
	QMapIterator<QString, QString> i(map);
	for (; i.hasNext();)
	{
		i.next();
		qDebug() << " " << i.key() << " " << i.value();
	}
	QMutableMapIterator<QString, QString>mi(map);
	if (mi.findNext("111"))
		mi.setValue("010");
	QMapIterator<QString, QString> modi(map);
	qDebug() << " ";
	for (; modi.hasNext();) {
		modi.next();
		qDebug() << " " << modi.key() << " " << modi.value();
	}
	return a.exec();
}
