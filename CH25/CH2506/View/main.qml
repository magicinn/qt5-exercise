﻿import QtQuick 2.7
import QtQuick.Controls 1.5
import QtQuick.Dialogs 1.2

ApplicationWindow {
    title: qsTr("世界艺术珍品")
    width: 780
    height: 320
    visible: true
    MainForm {
        anchors.fill: parent
        tableView.onClicked: {
            //单击选择列表项执行的代码
            tabView.currentIndex = tableView.currentRow //(a)

            //            scrolimg.source = "images/" + tabView.getTab(
            //                        tabView.currentIndex).title + ".jpg" //(b)
            scrolimg.source = "images/" + libraryModel.get(
                        tabView.currentIndex).fname + ".jpg"
            print(libraryModel.count)
        }
        tabView.onCurrentIndexChanged: {
            //切换TabView视图选项页执行的代码
            tableView.selection.clear() //(c)
            tableView.selection.select(tabView.currentIndex)
            scrolimg.source = "images/" + libraryModel.get(
                        tabView.currentIndex).fname + ".jpg"
            print(libraryModel.count)
        }
    }
}
