﻿#include "mouseevent.h"

MouseEvent::MouseEvent(QWidget *parent)
    : QMainWindow(parent)
{
    setWindowTitle(u8"鼠标事件");
    statusLabel=new QLabel;
    statusLabel->setText(u8"当前位置：");
    statusLabel->setFixedWidth(100);
    MousePosLabel=new QLabel;
    MousePosLabel->setText("");
    MousePosLabel->setFixedWidth(100);
    statusBar()->addPermanentWidget(statusLabel);
    statusBar()->addPermanentWidget(MousePosLabel);
    this->setMouseTracking(true);
    resize(400,200);
}

MouseEvent::~MouseEvent()
{

}

void MouseEvent::mousePressEvent(QMouseEvent *e)
{
    QString str="("+QString::number(e->x())+","+QString::number(e->y())+")";
    if(e->button()==Qt::LeftButton)
    {
        statusBar()->showMessage(u8"左键:"+str);
    }
    else if(e->button()==Qt::RightButton)
    {
        statusBar()->showMessage(u8"右键:"+str);
    }
    else if(e->button()==Qt::MidButton)
    {
        statusBar()->showMessage(u8"中键:"+str);
    }
}

void MouseEvent::mouseMoveEvent(QMouseEvent *e)
{
    MousePosLabel->setText("("+QString::number(e->x())+","+QString::number(e->y())+")");
}

void MouseEvent::mouseReleaseEvent(QMouseEvent *e)
{
      QString str=("("+QString::number(e->x())+","+QString::number(e->y())+")");
      statusBar()->showMessage(u8"释放在:"+str,3000);
}
