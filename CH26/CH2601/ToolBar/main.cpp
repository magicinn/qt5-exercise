﻿#include <QGuiApplication>
#include <QQmlApplicationEngine>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    int index=5;
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    QString url=QString("qrc:/main%1.qml").arg(index);
    engine.load(QUrl(url));

    return app.exec();
}
