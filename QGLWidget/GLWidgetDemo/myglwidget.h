﻿#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

//项目示例来源：https://www.jianshu.com/p/bccc565b5248

#include <QWidget>
#include<QOpenGLWidget>
#include<QOpenGLExtraFunctions>
#include<QOpenGLFunctions_3_3_Core>
#include<QOpenGLShaderProgram>
class MyGLWidget : public QOpenGLWidget,protected QOpenGLFunctions_3_3_Core
{
    Q_OBJECT

public:
    explicit MyGLWidget(QWidget *parent = 0);
    ~MyGLWidget();
protected:
   void initializeGL();
   void resizeGL(int w, int h);
   void paintGL();
private:
   QOpenGLShaderProgram shaderProgram;
};

#endif // MYGLWIDGET_H
