﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QUdpSocket>
#include <QNetworkInterface>
#include <QDateTime>
#include <QFile>
#include <QFileDialog>
#include "qdom.h"

class FileSrvDlg;
namespace Ui {
class MainWindow;
}
enum ChatMsgType{ChatMsg,OnLine,OffLine,SfileName,RefFile};
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void initMainWindow();
    void onLine(QString name,QString time);
    void offLine(QString name,QString time);
    void sendChatMsg(ChatMsgType msgType,QString rmtName="");
    QString getLocHostIp();
    QString getLocChatMsg();
    void recvFileName(QString name,QString hostip,QString rmtname,QString filename);
protected:
    void closeEvent(QCloseEvent *event);
private slots:
    void recvAndProcessChatMsg();
    void getSfileName(QString);
    void on_searchPushButton_clicked();

    void on_sendPushButton_clicked();

    void on_transPushButton_clicked();

private:
    Ui::MainWindow *ui;
    QUdpSocket *myUdpSocket;
    qint16 myUdpPort;
    FileSrvDlg *myfsrv;
    QString myname="";
    QString myFileName;
};

#endif // MAINWINDOW_H
