﻿#include "content.h"
#include<QHBoxLayout>
#include<QVBoxLayout>
#pragma execution_character_set("UTF-8")
Content::Content(QWidget *parent)
    : QFrame(parent)
{
    stack=new QStackedWidget(this);
    stack->setFrameStyle(QFrame::Panel|QFrame::Raised);
    baseInfo=new BaseInfo();
    contact=new Contact();
    detail=new Detail();
    stack->addWidget(baseInfo);
    stack->addWidget(contact);
    stack->addWidget(detail);
    AmendBtn=new QPushButton(tr("修改 "));
    ClosedBtn=new QPushButton(tr("关闭 "));
    QHBoxLayout *BtnLayout=new QHBoxLayout();
    BtnLayout->addStretch(1);
    BtnLayout->addWidget(AmendBtn);
    BtnLayout->addWidget(ClosedBtn);

    QVBoxLayout *RightLayout=new QVBoxLayout(this);
    RightLayout->setMargin(10);
    RightLayout->setSpacing(6);
    RightLayout->addWidget(stack);
    RightLayout->addLayout(BtnLayout);
}

Content::~Content()
{

}
