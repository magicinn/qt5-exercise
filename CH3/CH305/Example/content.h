﻿#ifndef CONENT_H
#define CONENT_H

#include <QWidget>
#include<QStackedWidget>
#include<QPushButton>
#include "baseinfo.h"
#include "contact.h"
#include "detail.h"
class Content : public QFrame
{
    Q_OBJECT

public:
    Content(QWidget *parent = 0);
    ~Content();
    QStackedWidget *stack;
    QPushButton *AmendBtn;
    QPushButton *ClosedBtn;
    BaseInfo *baseInfo;
    Contact *contact;
    Detail *detail;
};

#endif // CONENT_H
