﻿#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include<QIcon>
#include "htmlhandler.h"
#include "texthandler.h"
int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setOrganizationName("Shield");
    qmlRegisterType<HtmlHandler>("org.qtproject.easybooks",1,0,"HtmlHandler");
    qmlRegisterType<TextHandler>("org.qtproject.easybooks",1,0,"TextHandler");
    QGuiApplication app(argc, argv);
    app.setWindowIcon(QIcon(":/images/vieweye.png"));

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
