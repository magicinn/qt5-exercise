﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<QPrinter>
#include<QPrintDialog>
#include<QPrintPreviewDialog>
class MyChildWnd;
class QMdiSubWindow;
class QSignalMapper;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void docOpen();
    void docSave();
    void docSaveAs();
    void docUndo();
    void docRedo();
    void docCut();
    void docCopy();
    void docPaste();
    void docPrint();
    void docPrintPreview();
    void printPreview(QPrinter *);
    void textBold();
    void textItalic();
    void textUnderline();
    void textFamily(const QString &fmly);
    void textSize(const QString &ps);
    void textColor();
    void paraStyle(int sidx);

private slots:

    void on_aboutAction_triggered();

    void on_aboutQtAction_triggered();

    void on_newAction_triggered();

    void refreshMenus();

    void addSubWndListMenu();

    void on_closeAction_triggered();

    void on_closeAllAction_triggered();

    void on_tileAction_triggered();

    void on_cascadeAction_triggered();

    void on_nextAction_triggered();

    void on_previousAction_triggered();
    void on_openAction_triggered();

    void on_saveAction_triggered();

    void on_saveAsAction_triggered();

    void on_undoAction_triggered();

    void on_redoAction_triggered();

    void on_cutAction_triggered();

    void on_copyAction_triggered();

    void on_pastAction_triggered();

    void on_boldAction_triggered();

    void on_italicAction_triggered();

    void on_underlineAction_triggered();

    void on_fontComboBox_activated(const QString &arg1);

    void on_sizeComboBox_activated(const QString &arg1);

    void on_leftAlignAction_triggered();

    void on_centerAction_triggered();

    void on_rightAlignAction_triggered();

    void on_justifyAction_triggered();

    void on_colorAction_triggered();

    void on_styleComboBox_activated(int index);

    void on_printAction_triggered();

    void on_printPreviewAction_triggered();

protected:
    void closeEvent(QCloseEvent *event);

private:
    Ui::MainWindow *ui;
    void initMainWindow();
    void docNew();
    void formatEnabled();
    MyChildWnd *activateChildWnd();
    QSignalMapper *myWndMapper;
    QMdiSubWindow *findChildWnd(const QString &docName);
    void textAlign(QAction *act);
};

#endif // MAINWINDOW_H
