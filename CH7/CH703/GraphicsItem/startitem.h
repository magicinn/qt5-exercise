﻿#ifndef STARTITEM_H
#define STARTITEM_H

#include <QObject>
#include<QGraphicsItem>
#include<QPainter>
class StartItem : public QGraphicsItem
{
public:
    StartItem();
    QRectF boundingRect()const;
    void paint(QPainter *painter,const QStyleOptionGraphicsItem*,QWidget *);
private:
    QPixmap pix;

};

#endif // STARTITEM_H
