﻿#include "modelex.h"

ModelEx::ModelEx(QObject *parent):QAbstractTableModel (parent)
{
    armyMap[1]=u8"空军";
    armyMap[2]=u8"海军";
    armyMap[3]=u8"陆军";
    armyMap[4]=u8"海军陆战队";
    weaponTypeMap[1]=u8"轰炸机";
    weaponTypeMap[2]=u8"战斗机";
    weaponTypeMap[3]=u8"航空母舰";
    weaponTypeMap[4]=u8"驱逐舰";
    weaponTypeMap[5]=u8"直升机";
    weaponTypeMap[6]=u8"坦克";
    weaponTypeMap[7]=u8"两栖攻击舰";
    weaponTypeMap[8]=u8"两栖战车";
    populateModel();
}
void ModelEx::populateModel()
{
    header<<u8"军种"<<u8"种类"<<u8"武器";
    army<<1<<2<<3<<4<<2<<4<<3<<1;
    weaponType<<1<<3<<5<<7<<4<<8<<6<<2;
    weapon<<tr("B-2")<<u8"尼米兹级"<<u8"阿帕奇"<<u8"黄蜂级"
            <<u8"阿利伯克级"<<tr("AAAV")<<tr("M1A1")<<tr("F-22");
}
int ModelEx::columnCount(const QModelIndex &parent) const
{	 return 3;	   }
int ModelEx::rowCount(const QModelIndex &parent) const
{
    return army.size();
}
QVariant ModelEx::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    if(role==Qt::DisplayRole)				//(a)
    {
        switch(index.column())
        {
        case 0:
            return armyMap[army[index.row()]];
            break;
        case 1:
            return weaponTypeMap[weaponType[index.row()]];
            break;
        case 2:
            return weapon[index.row()];
        default:
            return QVariant();
        }
    }
    return QVariant();
}
QVariant ModelEx::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role==Qt::DisplayRole&&orientation==Qt::Horizontal)
           return header[section];
    return QAbstractTableModel::headerData(section,orientation,role);
}
