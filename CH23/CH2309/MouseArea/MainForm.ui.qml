﻿import QtQuick 2.7

Rectangle {
    property alias mouseArea: mouseArea

    width: 400
    height: 400

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }
    Rect{
        x:25
        y:25
        opacity:(360.0-x)/360
    }
}
