﻿import QtQuick 2.7
import QtQuick.Window 2.2

Window {
    visible: true
    width: 360
    height: 360
    title: qsTr("Image")

    MainForm {
        anchors.fill: parent
        mouseArea.onClicked: {
            console.log(qsTr('好啦好啦你点过了'))
        }
    }
}
